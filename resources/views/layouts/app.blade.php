<!DOCTYPE html>
<html x-data="data()" lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>
    @include('includes.meta')

    <title>@yield('title')</title>

    @stack('before-style')

    @include('includes.style')

    @stack('after-style')

  </head>
    <body class="hold-transition layout-top-nav">
      <div class="wrapper">
        @include('includes.header')
        @include('sweetalert::alert')

        @yield('content')

        @include('includes.aside')

        @include('includes.footer')


      </div>
      @stack('before-script')

      @include('includes.script')

      @stack('after-script')
    </body>
</html>
