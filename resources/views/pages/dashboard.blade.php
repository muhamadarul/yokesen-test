@extends('layouts.app')

@section('title', 'Dashboard')

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0"> Dashboard <small>List User</small></h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item active">Dashboard</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <div class="content">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-header">
              <form action="{{ url()->current() }}" method="get">
                @csrf
                <div class="row">
                  <div class="col-md-6">
                    <h3 class="card-title">Data User</h3>
                    <br>
                  </div>
                  <div class="col-md-4">
                    <input type="search" name="keyword" value="{{ request('keyword') }}" class="form-control float-sm-right" placeholder="Search name or email ...">
                  </div>
                  <div class="col-md-2">
                    <button type="submit" name="button" class="btn btn-primary">Search</button>
                    <br>
                  </div>
                </div>
              </form>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table class="table table-bordered table-responsive-lg-12">
                <thead>
                  <tr>
                    <th>Photo</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Address</th>
                    <th>Phone Number</th>
                    <th>Created At</th>
                  </tr>
                </thead>
                <tbody>
                  @forelse ($data as $row)
                    <tr>
                      @if ($row->photo != NULL)
                        <td>
                          <img class="profile-user-img img-fluid img-circle"
                             src="{{ url(Storage::url($row->photo)) }}"
                             alt="User profile picture">
                        </td>
                      @else
                        <td>
                          <img class="profile-user-img img-fluid img-circle"
                               src="{{ asset('images/default.png') }}"
                               alt="User profile picture">
                        </td>
                      @endif
                        <td>{{ $row->name }}</td>
                        <td>{{ $row->email }}</td>
                        <td>{{ $row->address }}</td>
                        <td>{{ $row->phone_number }}</td>
                        <td>{{ $row->created_at }}</td>
                    <tr>
                  @empty
                    <tr>
                      <td>No Data Available</td>
                    </tr>
                  @endforelse
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
            <div class="card-footer clearfix">
              <p>Current Page: {{ $data->currentPage() }}</p>
              <p>Total Data: {{ $data->total() }}</p>
              <p>Data Per Page: {{ $data->perPage() }}</p>
              {{ $data->links() }}
            </div>
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col-6 -->

      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection
