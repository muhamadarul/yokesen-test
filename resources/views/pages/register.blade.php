@extends('layouts.app')

@section('title', 'Register')

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0"> Register <small>User</small></h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item active">Register User</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <div class="content">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-lg-6">
          <div class="card">
            <div class="card-body register-card-body">
              <form method="post" action="{{ route('register.store') }}">
                @csrf
                <div class="input-group mb-3">
                  <input type="text" name="name" class="form-control @error('name')
                  is-invalid @enderror" placeholder="Full name" value="{{ old('name') }}" required>
                  <div class="input-group-append">
                    <div class="input-group-text">
                      <span class="fas fa-user"></span>
                    </div>
                  </div>
                  @if($errors->has('name'))
                  <div class="invalid-feedback">
                    {{ $errors->first('name') }}
                  </div>
                  @endif
                </div>
                <div class="input-group mb-3">
                  <input type="email" name="email" class="form-control @error('email')
                  is-invalid @enderror" placeholder="Email" value="{{ old('email') }}" required>
                  <div class="input-group-append">
                    <div class="input-group-text">
                      <span class="fas fa-envelope"></span>
                    </div>
                  </div>
                  @if($errors->has('email'))
                  <div class="invalid-feedback">
                    {{ $errors->first('email') }}
                  </div>
                  @endif
                </div>
                <div class="input-group mb-3">
                  <textarea name="address" class="form-control @error('address')
                  is-invalid @enderror" placeholder="Address" required>{{ old('address') }}</textarea>
                  <div class="input-group-append">
                    <div class="input-group-text">
                      <span class="fas fa-address-card"></span>
                    </div>
                  </div>
                  @if($errors->has('address'))
                  <div class="invalid-feedback">
                    {{ $errors->first('address') }}
                  </div>
                  @endif
                </div>
                <div class="input-group mb-3">
                  <input name="phone_number" class="form-control @error('phone_number')
                  is-invalid @enderror" placeholder="Phone Number" value="{{ old('phone_number') }}" required>
                  <div class="input-group-append">
                    <div class="input-group-text">
                      <span class="fas fa-phone"></span>
                    </div>
                  </div>
                  @if($errors->has('phone_number'))
                  <div class="invalid-feedback">
                    {{ $errors->first('phone_number') }}
                  </div>
                  @endif
                </div>
                <div class="input-group mb-3">
                  <input type="password" name="password" class="form-control @error('password')
                  is-invalid @enderror" placeholder="Password" value="{{ old('password') }}" required>
                  <div class="input-group-append">
                    <div class="input-group-text">
                      <span class="fas fa-lock"></span>
                    </div>
                  </div>
                  @if($errors->has('password'))
                  <div class="invalid-feedback">
                    {{ $errors->first('password') }}
                  </div>
                  @endif
                </div>
                <div class="input-group mb-3">
                  <input type="password" name="password_confirmation" class="form-control @error('password_confirmation')
                  is-invalid @enderror" placeholder="Confirm password" value="{{ old('password_confirmation') }}" required>
                  <div class="input-group-append">
                    <div class="input-group-text">
                      <span class="fas fa-lock"></span>
                    </div>
                  </div>
                  @if($errors->has('password_confirmation'))
                  <div class="invalid-feedback">
                    {{ $errors->first('password_confirmation') }}
                  </div>
                  @endif
                </div>
                <div class="row">
                  <div class="col-8">
                  </div>
                  <!-- /.col -->
                  <div class="col-4">
                    <button type="submit" class="btn btn-primary btn-block">Register</button>
                  </div>
                  <!-- /.col -->
                </div>
              </form>
              <a href="{{ route('login') }}" class="text-center">I already register</a>
            </div>
            <!-- /.form-box -->
          </div><!-- /.card -->

        </div>
        <!-- /.col-md-12 -->
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection
