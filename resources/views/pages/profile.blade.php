@extends('layouts.app')

@section('title', 'Profile')

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0"> Profile <small> User</small></h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item active">Profile User</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <div class="content">
    <div class="container">
      <div class="row">
        <div class="col-lg-4">
          <!-- Profile Image -->
          <div class="card card-primary card-outline">
            <div class="card-body box-profile">
              <div class="text-center">
                @if (auth()->user()->photo != NULL)
                  <img class="profile-user-img img-fluid img-circle"
                       src="{{ url(Storage::url(auth()->user()->photo)) }}"
                       alt="User profile picture">
                @else
                  <img class="profile-user-img img-fluid img-circle"
                       src="{{ asset('images/default.png') }}"
                       alt="User profile picture">
                @endif

              </div>

              <h3 class="profile-username text-center">{{ auth()->user()->name; }}</h3>

              <p class="text-muted text-center">{{ auth()->user()->phone_number; }}</p>

              <ul class="list-group list-group-unbordered mb-3">
                <li class="list-group-item">
                  <b>Address</b> <p class="float-right">{{ auth()->user()->address; }}</p>
                </li>
              </ul>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col-4 -->
        <div class="col-lg-8">
          <!-- Update Profile -->
          <div class="card">
            <div class="card-header">
              Update User
            </div>
            <div class="card-body">
              <form class="" action="{{ route('profile.update') }}" method="post" enctype="multipart/form-data">
                @csrf
                @method('PUT')

                <div class="input-group mb-3">
                  <input type="text" name="name" class="form-control @error('name')
                  is-invalid @enderror" placeholder="Full name" value="{{ auth()->user()->name; }}" required>
                  <div class="input-group-append">
                    <div class="input-group-text">
                      <span class="fas fa-user"></span>
                    </div>
                  </div>
                  @if($errors->has('name'))
                  <div class="invalid-feedback">
                    {{ $errors->first('name') }}
                  </div>
                  @endif
                </div>

                <div class="input-group mb-3">
                  <input type="email" name="email" class="form-control @error('email')
                  is-invalid @enderror" placeholder="Email" value="{{ auth()->user()->email;}}" required>
                  <div class="input-group-append">
                    <div class="input-group-text">
                      <span class="fas fa-envelope"></span>
                    </div>
                  </div>
                  @if($errors->has('email'))
                  <div class="invalid-feedback">
                    {{ $errors->first('email') }}
                  </div>
                  @endif
                </div>

                <div class="input-group mb-3">
                  <textarea name="address" class="form-control @error('address')
                  is-invalid @enderror" placeholder="Address" required>{{ auth()->user()->address; }}</textarea>
                  <div class="input-group-append">
                    <div class="input-group-text">
                      <span class="fas fa-address-card"></span>
                    </div>
                  </div>
                  @if($errors->has('address'))
                  <div class="invalid-feedback">
                    {{ $errors->first('address') }}
                  </div>
                  @endif
                </div>

                <div class="input-group mb-3">
                  <input name="phone_number" class="form-control @error('phone_number')
                  is-invalid @enderror" placeholder="Phone Number" value="{{ auth()->user()->phone_number; }}" required>
                  <div class="input-group-append">
                    <div class="input-group-text">
                      <span class="fas fa-phone"></span>
                    </div>
                  </div>
                  @if($errors->has('phone_number'))
                  <div class="invalid-feedback">
                    {{ $errors->first('phone_number') }}
                  </div>
                  @endif
                </div>

                <div class="input-group mb-3">
                  <input type="file" name="photo" class="form-control @error('photo')
                  is-invalid @enderror" placeholder="Password" value=""  accept="image/jpeg,image/gif,image/png" >
                  <div class="input-group-append">
                    <div class="input-group-text">
                      <span class="fas fa-user"></span>
                    </div>
                  </div>
                  @if($errors->has('photo'))
                  <div class="invalid-feedback">
                    {{ $errors->first('photo') }}
                  </div>
                  @endif
                </div>
                <button type="submit" name="button" class="btn btn-primary">Update Profile</button>
              </form>
            </div>
            <!-- /.card body -->
          </div>

          {{-- update password --}}
          <div class="card">
            <div class="card-header">
              Change Password
            </div>
            <div class="card-body">
              <form class="" action="{{ route('change.password') }}" method="post">
                @csrf
                @method('PUT')
                <div class="input-group mb-3">
                  <input type="password" name="current_password" class="form-control @error('current_password')
                  is-invalid @enderror" placeholder="Old Password" value="" required>
                  <div class="input-group-append">
                    <div class="input-group-text">
                      <span class="fas fa-key"></span>
                    </div>
                  </div>
                  @if($errors->has('current_password'))
                  <div class="invalid-feedback">
                    {{ $errors->first('current_password') }}
                  </div>
                  @endif
                </div>

                <div class="input-group mb-3">
                  <input type="password" name="password" class="form-control @error('password')
                  is-invalid @enderror" placeholder="New Password" value="" required>
                  <div class="input-group-append">
                    <div class="input-group-text">
                      <span class="fas fa-key"></span>
                    </div>
                  </div>
                  @if($errors->has('password'))
                  <div class="invalid-feedback">
                    {{ $errors->first('password') }}
                  </div>
                  @endif
                </div>

                <div class="input-group mb-3">
                  <input type="password" name="password_confirmation" class="form-control @error('password_confirmation')
                  is-invalid @enderror" placeholder="Confirm Password" value="" required>
                  <div class="input-group-append">
                    <div class="input-group-text">
                      <span class="fas fa-key"></span>
                    </div>
                  </div>
                  @if($errors->has('password_confirmation'))
                  <div class="invalid-feedback">
                    {{ $errors->first('password_confirmation') }}
                  </div>
                  @endif
                </div>
                <button type="submit" name="button" class="btn btn-primary">Change Password</button>
              </form>
            </div>
            <!-- /.card body -->
          </div>

        </div>
        <!-- /.col-8 -->

      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection
