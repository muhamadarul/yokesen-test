<!-- Navbar -->
<nav class="main-header navbar navbar-expand-md navbar-light navbar-white">
  <div class="container">
    <a href="" class="navbar-brand">
      {{-- <img src="../../dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8"> --}}
      <span class="brand-text font-weight-light">Yokesen Test</span>
    </a>
    <!-- Right navbar links -->
    <ul class="order-1 order-md-3 navbar-nav navbar-no-expand ml-auto">
      <li class="nav-item">
        <a href="{{ route('dashboard') }}" class="nav-link {{ request()->is('/')|| request()->is('dashboard') ? 'active' : ''}}">Dashboard</a>
      </li>
      @auth
        <li class="nav-item dropdown">
          <a id="dropdownSubMenu1" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle">Welcome, {{ auth()->user()->name; }}</a>
          <ul aria-labelledby="dropdownSubMenu1" class="dropdown-menu border-0 shadow">
            <li><a href="{{route('profile')}}" class="dropdown-item">Profile</a></li>
            <li><a href="{{route('logout')}}" class="dropdown-item" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a></li>
            <form action="{{route('logout')}}" method="post" id="logout-form" style="display: none;">
            @csrf
            </form>
          </ul>
        </li>
      @else
        <li class="nav-item">
          <a href="{{ route('register') }}" class="nav-link {{ request()->is('register') ? 'active' : ''}}">Register</a>
        </li>
        <li class="nav-item">
          <a href="{{ route('login') }}" class="nav-link {{ request()->is('login') ? 'active' : ''}}">Login</a>
        </li>
      @endauth
    </ul>
  </div>
</nav>
<!-- /.navbar -->
