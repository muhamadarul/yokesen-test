<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;

class UserModelFactory extends Factory
{
  /**
  * The name of the factory's corresponding model.
  *
  * @var string
  */
 protected $model = User::class;

 /**
  * Define the model's default state.
  *
  * @return array
  */
    public function definition()
    {
        return [
          'name' => $this->faker->name(),
          'email' => $this->unique()->safeEmail(),
          'address' => $this->faker->address(),
          'phone_number'    => '000000000000',
          'password'     => Hash::make('admin@12345'),
        ];
    }
}
