<?php

namespace App\Http\Controllers;
use Alert;
use DB;
use Illuminate\Http\Request;
use App\Models\User;


class DashboardController extends Controller
{
    public function index(Request $request)
    {
      $pagination = 20;
      //mengambil data user dari database
      $data    = User::when($request->keyword, function ($query) use ($request) {
                     $query->where('name', 'like', "%{$request->keyword}%")
                           ->orWhere('email', 'like', "%{$request->keyword}%");
                         })->orderBy('name', 'asc')->paginate($pagination);

       $data->appends($request->only('keyword'));

      // $data = DB::table('users')
      //        ->orderBy('name', 'asc')
      //        ->paginate(20);

      //mengirim data user ke view dashboard
      return view('pages.dashboard', compact('data'))->with('i', (request()->input('page', 1) - 1) * $pagination);
    }

}
