<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;


use Alert;
use DB;
use Auth;
use File;

class ProfileController extends Controller
{
    public function __construct()
    {
      $this->middleware('auth');
    }

    public function index()
    {
      return view('pages.profile');
    }

    public function update(Request $request)
    {

      // $requestUser = $request->all();
      $requestUser =  $request->validate([
          'name' =>  ['required', 'string', 'max:255'],
          'email' => ['required', 'string', 'max:255', 'email', Rule::unique('users')->where(function ($query) {
                return $query->where('id', '<>', Auth::user()->id);
                // '<> : tidak sama dengan'
            })
          ],
          'address' => ['required', 'string'],
          'phone_number' => ['required', 'regex:/^([0-9\s\-\+\(\)]*)$/', 'max:13'],
          'photo' => ['nullable', 'file', 'max:1024'],
        ]);

        // dd($requestUser);

      //get Photo
        $get_photo = User::where('id', Auth::user()->id)->first();

        //detele old photo
        if (isset($requestUser['photo'])) {
          $data =  'storage/'.$get_photo['photo'];
            if (File::exists($data)) {
              File::delete($data);
            }else {
              //detail delete
              File::delete('storage/app/public/'.$get_photo['photo']);
            }
        }


        //store photo to storage
        if (isset($requestUser['photo'])) {
          $requestUser['photo'] = $request->file('photo')
                                          ->store('assets/images', 'public');
        }

        // $requestUser['password'] = Hash::make($requestUser['password']);
        //proccess saving/updating to users
        $user = User::find(Auth::user()->id);
        $user->update($requestUser);
        toast()->success('Profile Updated !');
        return back();
    }

    public function change_password(Request $request)
    {
      $request->validate([
          'current_password' => ['required'],
          'password' => ['required','min:6' , 'confirmed','different:current_password'],

        ]);

      if (!Hash::check($request->current_password, auth()->user()->password)) {
        throw ValidationException::withMessages([
            'current_password' => 'Your current password does not match with our record',
        ]);
      }

      // dd($request->all());
      $data = User::find(Auth::user()->id);
      $data->update(['password' => Hash::make($request->password)]);
      toast()->success('Password Updated !');
      return back();

    }
}
