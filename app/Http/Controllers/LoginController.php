<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use Alert;


class LoginController extends Controller
{
    public function index()
    {
      return view('pages.login');
    }
    public function authenticate(Request $request)
    {
        $credentials = $request->validate([
            'email' => ['required', 'email:dns'],
            'password' => ['required'],
        ]);

        if (Auth::attempt($credentials)) {
            $request->session()->regenerate();

            toast()->success('Success Login !');

            return redirect()->intended('profile');
        }
        toast()->error('Login Failed !');
        return back();
    }

    public function logout(Request $request)
    {
      Auth::logout();

      $request->session()->invalidate();

      $request->session()->regenerateToken();

      toast()->success('Success Logout !');

      return redirect('/');
    }
}
