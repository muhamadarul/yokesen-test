<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use Alert;

class RegisterController extends Controller
{
    public function index()
    {
        return view('pages.register');
    }

    public function store(Request $request)
    {
      $data = $request->validate([
        'name' =>  ['required', 'string', 'max:255'],
        'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
        'address' => ['required', 'string'],
        'phone_number' => ['required', 'regex:/^([0-9\s\-\+\(\)]*)$/', 'max:13'],
        'password' =>  ['required','confirmed','min:6'],
      ]);

      $data['password'] = Hash::make($data['password']);
      User::create($data);
      toast()->success('Sucess Registered');
      return redirect()->route('dashboard');
    }
}
